import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { Ng4SuggestComponent } from './components/ng4-suggest/ng4-suggest.component';


@NgModule({
  declarations: [
    AppComponent,
    Ng4SuggestComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
