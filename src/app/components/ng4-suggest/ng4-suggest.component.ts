// Used for autocomplete of a component, it works only with HTMLInputElements
import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    ElementRef
} from '@angular/core';

@Component({
    selector: 'ng4-suggest',
    templateUrl: './ng4-suggest.component.html',
    styleUrls: ['./ng4-suggest.component.css']
})
export class Ng4SuggestComponent implements OnInit {
    @Input() inputTarget: HTMLInputElement;
    @Input() list: any[];
    @Input() key: string;

    @Output() itemSelected = new EventEmitter<any>();
    @Output() clickOutside = new EventEmitter<any>();

    preparedList: any[];
    listVisible: boolean;
    keyTimeoutFunc: any;// for the onkeyup event
    targetInputLeft: any;

    constructor(private ref: ElementRef) {
        this.list = [];

        // click outside the list:
        window.addEventListener('click', () => {
            this.listVisible = false;

            if(this.clickOutside) {
                this.clickOutside.emit(this.inputTarget);
            }
        });
    }

    ngOnInit() {
        // css workaround for horizontal alignment:
        this.targetInputLeft = this.inputTarget.getBoundingClientRect().left + "px";

        if(!this.inputTarget) {
            console.error('Please give a target input reference!');
            return;
        }

        // events of an intuitive behavior:
        this.inputTarget.onfocus = (evt: any) => {
            this.inputTarget.onkeyup(new KeyboardEvent("onkeyup"));
        };

        this.inputTarget.onkeyup = () => {
            let inputText = this.inputTarget.value.toLowerCase().trim();

            if(inputText.trim().length > 0) {
                this.listVisible = false;
                if(this.keyTimeoutFunc) {
                    clearTimeout(this.keyTimeoutFunc);
                }
    
                this.keyTimeoutFunc = setTimeout(() => {
                    let matchRegex = new RegExp("^" + inputText);
    
                    // look for matches:
                    let atLeastOne = false;
                    for(let item of this.preparedList) {
                        let itemText = item.content.toLowerCase().trim();
                        if(itemText.match(matchRegex)) {
                            item.visible = true;
                            atLeastOne = true;
                        } else {
                            item.visible = false;
                        }
    
                    }
                    if(atLeastOne) {
                        this.listVisible = true;
                    }
                }, 400);
            }
        };

        this.listVisible = false;
        this.preparedList = [];

        // convert to a more useful array:
        for (let i = 0; i < this.list.length; i++) {
            let e = this.list[i];// itshould be an object or a string

            // the specified key should be in all items:
            if(this.key && !e[this.key]) {
                console.error(`No key '${this.key}' found in the object at index ${i}`);
                return;
            }

            this.preparedList.push({
                visible: true,
                reference: e,
                content: this.key ? e[this.key] : e
            });
        }
        let i = 0;
    }

    selectItem = (item: any) => {
        this.listVisible = false;
        this.inputTarget.value = item.content;
        this.itemSelected.emit(item.reference);
    };
}
