## ng4-suggest

An Angular 4 directive for HTML Input suggestions.

### Installation

```
npm install ng4-suggest --save
```

Import the component in app.module.ts:

```javascript
...
import { Ng4SuggestComponent } from 'ng4-suggest';

@NgModule({
  declarations: [
    ...
    Ng4SuggestComponent
    ...
  ],
  ...
})
export class AppModule { }
```

### Usage

```html
<input type="text" #user />
<ng4-suggest [inputTarget]="user" [list]="['Juan', 'Pedro', 'Arnold']"></ng4-suggest>
```

### Using a more realistic list

Let's say we have this list of users:

```json
let users = [{
    "id": "1",
    "name": "Mónica",
    "role": "revisor",
    "lastname": "Hernández",
    "lastname2": "Cárdenas",
    "email": "monica@monica.com"
}, {
    "id": "2",
    "name": "Eréndira",
    "role": "auditado",
    "lastname": "Beltrán",
    "lastname2": "Lara",
    "email": "herendira@herendira.com"
}, {
    "id": "3",
    "name": "Dana",
    "role": "auditado",
    "lastname": "Gómez",
    "lastname2": "Gómez",
    "email": "dana@dana.com"
}];
```

The 'key' input will be needed in order to display the 'name' property:

```html
<ng4-suggest
[inputTarget]="user"
[list]="users"
key="name"></ng4-suggest>
```

### Events

Just add the itemSelected listener:

```html
<ng4-suggest
[inputTarget]="user"
[list]="users"
(itemSelected)="selectUser($event)"
key="name"></ng4-suggest>
```

```$event``` will contain the item selected, like Mónica:

```json
{
    "id": "1",
    "name": "Mónica",
    "role": "revisor",
    "lastname": "Hernández",
    "lastname2": "Cárdenas",
    "email": "monica@monica.com"
}
```

(in the first example, ```$event``` will contain just a string)
